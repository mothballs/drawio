# Introduction 
A central repository used to store all vcass related drawio diagrams. 

# Getting Started
Users wishing to use this repo will need two tools, running on their own system:
1.	[Git](https://git-scm.com/) a distributed version control system,
1.	[Drawio](https://github.com/jgraph/drawio-desktop/releases) a open source application for creating diagrams.

# Contribute
Please ensure your vcaas related diagrams are created in drawio as xml documents, and ensure they follow the appropriate file naming standard. 

# File Naming Standard
1.	**cr** - a change request shows the existing _as is_ and target _to be_ designs,
1.	**design** - a description of sufficient detail to allow a component or feature to be understood, built or changed,
1.	**scope** - a high level why, when, who, what, where and how definition of a service or product,
1.	**view** - provides a more formal description of a process, system or technology

# List of drawio directories
* **aws** _holds the bid diagrams used for amazon delivery in the USA_
* **quick**  _a mobile app and its backend for frontend, built as a set of reusable features_
* **everything-as-a-service** _extended openbanking services across multiple areas_
* **kdd** _key design decisions'_
* **open-banking**
* **phoenix**
* **psp** _payment service provider to support topup journies_
* **vcaas** _virtual cards and account service_
